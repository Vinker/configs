" Disable vi compatibility mode
set nocompatible

" Enable syntax highlighting
syntax on

" Enable filetype detection for plugins and indentation options
filetype plugin indent on

" Force encoding to utf-8
set encoding=utf-8

" Disable swapfiles
set noswapfile



" -------------------------------------------------------------------
" Usability options

" Allow backspacing over autoindent
set backspace=indent,eol,start

" Keep the last indent if not a filetype-specific indenting
set autoindent

" Display the cursor position
set ruler

" Display line numbers on the left
set number

" Use <F11> to toggle between 'paste' and 'nopaste'
set pastetoggle=<F11>

"--------------------------------------------------------------------
" Indentation options


